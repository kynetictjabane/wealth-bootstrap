﻿(function () {
    module.exports = function (grunt) {
        grunt.initConfig({
            bowerDirectory: require('bower').config.directory,
            less: {
                compile: {
                    options: {
                        compress: false,
                        paths: ['less', 'tmp', '<%= bowerDirectory %>/bootstrap/less']
                    },
                    files: {
                        'dist/css/wealth-bootstrap.css': ['less/wealth-bootstrap.less']
                    }
                }
            },
            recess: {
                dist: {
                    options: {
                        compile: true
                    },
                    files: {
                        'dist/css/wealth-bootstrap.css': ['dist/css/wealth-bootstrap.css']
                    }
                }
            },
            watch: {
                less: {
                    files: ['less/*.less'],
                    tasks: ['copy', 'less:compile', 'clean'],
                    options: {
                        livereload: true
                    }
                },
                cssmin: {
                    files: ['dist/css/wealth-bootstrap.css'],
                    tasks: ['cssmin:minify']
                }
            },
            cssmin: {
                minify: {
                    expand: true,
                    cwd: 'dist/css',
                    src: ['*.css', '!*.min.css'],
                    dest: 'dist/css',
                    ext: '.min.css'
                }
            },
            connect: {
                serve: {
                    options: {
                        port: '8000',
                        hostname: 'localhost'
                    }
                }
            },
            open: {
                server: {
                    url: 'http://localhost:8000/'
                }
            },
            copy: {
                bootstrap: {
                    files: [
                      {
                          expand: true,
                          cwd: '<%= bowerDirectory %>/bootstrap/less',
                          src: ['bootstrap.less'],
                          dest: 'tmp/'
                      }, {
                          expand: true,
                          cwd: '<%= bowerDirectory %>/bootstrap/dist/fonts',
                          src: ['*'],
                          dest: 'dist/fonts'
                      }, {
                          expand: true,
                          cwd: '<%= bowerDirectory %>/bootstrap/dist/js',
                          src: ['*'],
                          dest: 'dist/js'
                      }
                    ]
                }
            },
            clean: ['tmp']
        });
        grunt.loadNpmTasks('grunt-contrib-less');
        grunt.loadNpmTasks('grunt-recess');
        grunt.loadNpmTasks('grunt-contrib-watch');
        grunt.loadNpmTasks('grunt-contrib-cssmin');
        grunt.loadNpmTasks('grunt-contrib-copy');
        grunt.loadNpmTasks('grunt-contrib-clean');
        grunt.loadNpmTasks('grunt-contrib-connect');
        grunt.loadNpmTasks('grunt-open');
        grunt.registerTask('default', ['copy', 'less', 'recess', 'cssmin', 'clean']);
        grunt.registerTask('build', ['copy', 'less', 'recess', 'cssmin', 'clean']);
        return grunt.registerTask('server', ['copy', 'less', 'recess', 'cssmin', 'clean', 'connect', 'open:server', 'watch']);
    };
}).call(this);